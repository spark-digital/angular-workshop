import { Component, OnInit } from '@angular/core';
import {Quote} from '../../models/quote';
import {QuotesService} from '../quotes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public appQuote: Quote;
  constructor(private quotesService: QuotesService) { }

  changeQuote() {
    console.log('Quote changed');
    // this.appQuote = new Quote('Hello world', 'anonymous', '');
    // this.appQuote.author = 'me';
    // this.quotesService.getQuote().subscribe(data => {
    //   this.appQuote = data;
    // });
    this.quotesService.getOtherQuote().subscribe(data => {
      this.appQuote = data;
    });
  }
  ngOnInit() {
    this.appQuote = new Quote(
      'Respect is one of the greatest expressions of love.',
      'Miguel Angel Ruiz',
      'respect');
  }
}
