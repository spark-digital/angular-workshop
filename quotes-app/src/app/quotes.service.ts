import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Quote} from '../models/quote';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class QuotesService {

  constructor(private http: Http, private httpClient: HttpClient) {

  }

  getQuote(): Observable<Quote> {
    return this.http.get('https://talaikis.com/api/quotes/random')
      .map((quote: Response) => {
        return quote.json();
      });
  }

  getOtherQuote(): Observable<Quote> {
    return this.httpClient.get('https://talaikis.com/api/quotes/random');
  }

}
