import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-twowaybinding',
  templateUrl: './twowaybinding.component.html',
  styleUrls: ['./twowaybinding.component.css']
})
export class TwowaybindingComponent{

  name: string;

  toUpperCase() {
    this.name = this.name.toUpperCase();
  }

  toLowerCase() {
    this.name = this.name.toLowerCase();
  }

  constructor() { }


}
