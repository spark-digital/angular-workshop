import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'addtext'})
export class AddTextPipe implements PipeTransform {
  transform(value: string, toAdd: string): string {
    return value  + toAdd;
  }
}
