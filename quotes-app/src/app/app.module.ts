import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { QuoteComponent } from './quote/quote.component';
import { TwowaybindingComponent } from './twowaybinding/twowaybinding.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {QuotesService} from './quotes.service';
import {HttpClientModule} from '@angular/common/http';
import {AddTextPipe} from './add-text.pipe';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    QuoteComponent,
    TwowaybindingComponent,
    AddTextPipe,
    AboutComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)

  ],
  providers: [QuotesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
