import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Quote} from '../../models/quote';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements  OnInit, OnChanges {

  @Input() showButton: boolean;
  @Input() quote: Quote;
  @Output() onQuoteChange = new EventEmitter<any>();

  readAnother(event) {
    console.log(event);
    this.onQuoteChange.emit();
  }


  constructor( ) { }

  ngOnChanges(changes: SimpleChanges) {

    console.log(`ngOnChanges - data is ${this.quote}`);

        if (!changes['quote'].firstChange) {
          console.log(`Current value ${changes['quote'].currentValue['author']}`);
          console.log(`Previous value ${changes['quote'].previousValue['author']}`);
          console.log(`First change?  ${changes['quote'].firstChange}`);
        }

  }

  ngOnInit() {
    console.log(`ngOnInit  - data is ${this.quote}`);
  }

  ngDoCheck() {
    console.log("ngDoCheck");
  }

  ngAfterContentInit() {
    console.log("ngAfterContentInit");
  }

  ngAfterContentChecked() {
    console.log("ngAfterContentChecked");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy");
  }

}
