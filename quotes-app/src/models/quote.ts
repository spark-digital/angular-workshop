export class Quote {
  quote: string;
  author: string;
  cat: string;

  constructor(quote: string, author: string, cat: string) {
    this.quote = quote;
    this.author = author;
    this.cat = cat;
  }
}
