Angular Workshop
================

Your shiny brand new app (Create a new Angular App called �quotes-app�)
-----------------------------------------------------------------------

We�re gonna start up by installing the cli, creating a new application and running it (takes about 5 min):

```
npm install -g @angular/cli
ng new quotes-app
cd quotes-app
ng serve
```

Using your browser, navigate to http://localhost:4200


Create a quote component
------------------------

Next, we'll need a new component, so we're going to create it using the cli. Open a new terminal, 'cd' to the app directory and run:

```
ng generate component quote
```

That was easy.


Making our component somehow useful (Show the quote and the author in our Quote component)
------------------------------------------------------------------------------------------

We're gonna modify our new component, so it's useful for us. Open the recently created `src/app/quote/quote.component.ts` and replace it's contents with:

```
import { Component } from '@angular/core';

// Later we're going to explain this more in depth
@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent {
  // We define a public property for the class. This will be automatically available in the template
  // Defined using Typescript
  public quote: {quote: string, author: string, cat: string} = {
    quote: 'Respect is one of the greatest expressions of love.',
    author: 'Miguel Angel Ruiz',
    cat: 'respect'
  };
}
```

And then we're going to modify the template, to show some interesting info. Replace the `src/app/quote/quote.component.html` content with:

```
<h1>
  {{quote.quote}}
</h1>
<h3>
  {{quote.author}}
</h3>
```

Then instantiate the component in the main template, so open `src/app/app.component.html`, remove all contents and add:

```
<div class="app">
  <app-quote>
  </app-quote>
</div>
```

If you left your server running and the browser open, you should see the changes without any interaction. `ng serve` watches the files for changes and automatically refreshes the browser (with the help of a utility called BrowserSync).


Property Binding (Pass the quote object data as as an Input to the Quote component)
-----------------------------------------------------------------------------------

To make the component more dynamic we will pass the quote data through using a data binding type called 'Input'. So open your quote components controller (`src/app/quote/quote.component.ts`) and write:

```
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent {
  // Define a component input binding using the @Input decorator
  @Input() quote: {quote: string, author: string, cat: string};
}
```

Now open the main template file (`src/app/app.component.html`) and replace the component instantiation with:

```
 <app-quote [quote]="quote">
 </app-quote>
```

Lastly, open the main modules controller (`src/app/app.component.ts`) and inside the controller add:

```
  // Define a public property, that will be available in the app.component.html template
  public quote: {quote: string, author: string, cat: string} = {
    quote: 'Respect is one of the greatest expressions of love.',
    author: 'Miguel Angel Ruiz',
    cat: 'respect'
  };
```


Event Binding (Pass a function as the Output and have it execute when the user clicks it)
-----------------------------------------------------------------------------------------

To test Event binding we're going to change the quote components template (`src/app/quote/quote.component.html`) to contain (right below the actual quote):

```
<button (click)="readAnother()">Read Another</button>
```

Then change the quote controller (`src/app/quote/quote.component.ts`) as follows:

```
import { ..., Output, EventEmitter } from '@angular/core';
...
export class QuoteComponent {
  ...
  // Define a component output binding, able to emit an event
  @Output() onQuoteChange = new EventEmitter<any>();
  // Define a method to control the native 'click' event from the button
  readAnother(event) {
    this.onQuoteChange.emit();
  }
}
```

Add the event bind to the new-quote-btn instantiation (in `src/app/app.component.html`):

```
  <app-quote [quote]="quote" (onQuoteChange)="changeQuote()">
  </app-quote>
```

And lastly define the 'changeQuote' method in the parent component:

```
  public changeQuote() {
    // Redefine the quote property.
    // This was the only other quote I had in hand, printed on a sugar packet
    this.quote = {
      quote: 'Identidad propia.',
      author: 'Cabrales',
      cat: 'marketing'
    };
  }
```


Native Directives (Conditionally hide the button using *ngIf)
-------------------------------------------------------------

We'll add a built-in directive to our button, to control wheter it's shown or not. It will be bound to a property in the controller:

```
<button (click)="readAnother()" *ngIf="showButton">Read Another</button>
```

Then export it as an input in our controller:

```
    @Input() showButton: boolean;
```

And use it in the main template:

```
  <app-quote [quote]="quote" (onQuoteChange)="changeQuote()" [showButton]="true">
  </app-quote>
```


Part 2
=======================

Move the quotes variable assignment to the start of the ngOnInit method
------------------------------------------------------------------------------

We're gonna start by moving the quote variable assignment in `app.component.ts` to the ngOnInit method, to have all content initialized there. It's not much of a change now, but we'll use it further on.

```
import { Component, OnInit } from '@angular/core';
... 
export class AppComponent implements OnInit { 
  public quote: {quote: string, author: string, cat: string};

  public ngOnInit() {
    this.quote = {
      quote: 'Respect is one of the greatest expressions of love.',
      author: 'Miguel Angel Ruiz',
      cat: 'respect'
    };
  }
}
```

Now our content is initalized using the OnInit hook.


Create a quotes service
-----------------------

Let's create a service! We'll use it to fetch quotes from a public API. It's gonna be conveniently named QuotesService.

```
ng generate service quotes
```

Easy!

Change the quote by fetching a new one from the API using our new service
--------------------------------------------------------------------------

First, declare our new service as a dependency in `app.module.ts`. Also we'll need to import HttpModule and declare it in our modules' imports.

```
//app.module.ts
import { HttpModule } from '@angular/http';
import { QuotesService } from './quotes.service';
...
@NgModule({
  ...
  imports: [HttpModule],
  providers: [QuotesService]
  ...
})
export class AppModule { ... }
```

Then we�ll set up Http on our service by adding it as a parameter on the service's constructor, and create a public method on our service that fetches a random quote from the public API. Angualr provides the `http.get()` method, which returns an `Observable`. You can think of it as *an array which values arrive over time*. Basically it's a stream which you can listen on, to use the values that it emits, and manipulate those values using a lot of different methods provided by the `rxjs` library.

In this case, calling `http.get()` with the URL of the public quotes API will create an `Observable` which will emit a Response object which contains our quote. However, in order to use the JSON quote object with the same structure as ours, we need to take it from the raw response object. We do that by appending the `map()` method to our Observable, which basically allows us to execute some processing logic to transform each object that the Observable emits, and return that (instead of the original object) as a response. In our case, we'll just return the original response object but with the `.json()` method attached, so it will allow us to get the quote in JSON format.

Try it!

```
//quotes.service.ts
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class QuotesService {

  // Inject Http dependency and name it 'http'
  constructor(private http: Http) { }
 
  getQuote() {
    // Get the response from the server and convert each value to it's json form
    return this.http.get('https://talaikis.com/api/quotes/random')
      .map((quoteResponse: Response) => {
        return quoteResponse.json();
      });
  }
}
```

Finally, we use our service in our App Component (`app.component.ts `), by injecting it and calling its getQuote() method. Since it's an observable, we'll need to subscribe to it (listen to the values it emits) throught the `subscribe()` method. We pass a callback function to it that recieves the new values every time they come, and allow us to manipulate those values: in this case, we'll just assign that value to our `this.quote` variable so it updates it.

We'll also call this method on the ngOnInit so we'll get a quote from the API right from the start, instead of hardcoding the first quote we are shown.

```
//app.component.ts
import { QuotesService } from './quotes.service';
...
public constructor(private quotesService: QuotesService) {}

public ngOnInit() {
  this.changeQuote();
}
 
public changeQuote() {
  // Get the getQuote Observable and subscribe to it
  this.quotesService.getQuote()
    .subscribe((quote: any) => {
      // Every quote that arrives here is in JSON format (because of the map in the service) 
      this.quote = quote;
    });
}
```

All set! Test it!

In our Quote component, let�s uppercase the quote author using the built-in uppercase pipe
------------------------------------------------------------------------------------------

Just add ` | pipeline` to the interpolation where we show the quote's author (`quote.component.html`). 

```
//quote.component.html
<h1>
  {{quote.quote}}
</h1>
<h3>
  {{quote.author | uppercase}}
</h3>
<button (click)="readAnother()" *ngIf="showButton">Read Another</button>
```

Create the About component and the Home component
-------------------------------------------------

Ok, we'll have routing! But first, we need to create 2 components to route to: a new `about` component and a `home` component that will contain what's currently in our AppComponent. First, let's create them both! Run:

```
ng g component about
ng g component home 
```

Our About component is just to test that routing works, so you can just open `about.component.html` and write a nice message. That'll be it.

In the Home component we'll take all the quote related logic that's currently in the AppComponent. The idea is that the AppComponent will just work as an entry point for our router. So, after copying and pasting, the home component should look something like this:

```
//home.component.ts
import { Component, OnInit } from '@angular/core';
import {QuotesService} from './quotes.service';

export class HomeComponent implements OnInit {
	public quote: {quote: string, author: string, cat: string};
	public constructor(private quotesService: QuotesService) {}
	
	public ngOnInit() {
	   this.changeQuote();
	 }
	 
	public changeQuote() {
	   this.quotesService.getQuote()
	   .subscribe((quote: any) => {
	     this.quote = quote;
	   });
	 }
}
```

```
//home.component.html
<app-quote [quote]="quote" (onQuoteChange)="changeQuote()" [showButton]="true">
  </app-quote>
```

And the `AppComponent`, both its template and its component, should be empty except for component definition and instantiation.

Add routing to our app and test it by setting the desired route in the address bar
---------------------------------------------------------------------------------
Let�s set up our router by importing its dependencies and creating a routes map in the app module. We'll add imports for everything we need and create an array of Routes that defines our route map.

```
//app.module.ts
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
...
const appRoutes: Routes = [
 { path: '', component: HomeComponent },
 { path: 'about', component: AboutComponent },
];
```

We also have to tell our app to use the routing we configured by importing the `RouterModule` with the `forRoot(config)` method. This is a method that allows to define a certain configuration for an imported module, specified in the `config` parameter. In this case, we'll use it to pass our route map to the Router module. Add this in the `imports` array of the `AppModule`:

```
//app.module.ts 
imports: [
   BrowserModule,
   HttpModule,
   RouterModule.forRoot(
     appRoutes
   )
]
```

Finally, we need to tell our markup somewhere about this routing stuff! We'll have to add the `<router-outlet>` component in our `app.component.html`. This will instantiate the component that matched the active route (by default, as we have defined in our route map, it will be the `HomeComponent`).

Add a link to the app and test that it works
--------------------------------------------

Just add a link in our home component using the `<a>` tag, like any common link. Only one small difference: We use the `routerLink` directive (not `href`) for internal routing in the app. Just add it under the quote component on `home.component.html`:

```
<app-quote [quote]="quote" (onQuoteChange)="changeQuote()">
</app-quote>
<a routerLink="about"> About this app </a>
```

Done! Click it to see if it works. You can also go back and forth using the browser's arrows to see that works too.


APPENDIX A: CSS FILES
=====================

Here are some CSS files, to make your app look even more awesome.

`src/app/app.component.css`:

```
.app {
    display: flex;
    justify-content: center;
    align-content: center;
}
```

`src/app/quote/quote.component.css`:

```
:host {
    background: rgb(135,16,69);
    max-width: 250px;
    box-sizing: border-box;
    padding: 1em;
    color: #fefefe;
    font-family: serif;

    -moz-box-shadow: 2px 2px 15px #ccc;
    -webkit-box-shadow: 2px 2px 15px #ccc;
    box-shadow: 2px 2px 15px #ccc;
}

h1 {
    font-size: 1.8em;
    font-weight: normal;
}

h1:before,
h1:after {
    display: block;
    font-size: 3em;
    font-family: serif;
    margin-bottom: -.5em;
}

h1:before {
    content: '\201C';
}
h1:after {
    content: '\201D';
    text-align: center;
}

h3 {
    font-weight: normal;
    text-align: right;
}

button {
    margin-top: 1.3em;
    background: rgb(135, 16, 69);
    box-sizing: border-box;
    width: 100%;
    padding: 1.5em;
    box-shadow: none;
    border: 1px solid #ccc;
    font-size: 1em;
    color: #fff;
}
```